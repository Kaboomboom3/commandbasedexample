package org.frcteam2910.command_example.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.frcteam2910.command_example.commands.TankDriveCommand;

public class DrivetrainSubsystem extends Subsystem {

    private VictorSP leftMotorController = new VictorSP(0);
    private VictorSP rightMotorController = new VictorSP(1);

    public DrivetrainSubsystem() {
        rightMotorController.setInverted(true);
    }


    public void tankDrive(double left, double right) {
        leftMotorController.set(left);
        rightMotorController.set(right);
    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new TankDriveCommand(this));
    }
}
