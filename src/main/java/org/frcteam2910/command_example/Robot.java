package org.frcteam2910.command_example;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Scheduler;
import org.frcteam2910.command_example.subsystems.DrivetrainSubsystem;

public class Robot extends TimedRobot {
    private DrivetrainSubsystem drivetrain = new DrivetrainSubsystem();

    @Override
    public void robotInit() {
        OI.bindButtons(drivetrain);
    }

    @Override
    public void teleopPeriodic() {
        Scheduler.getInstance().run();
    }
}
