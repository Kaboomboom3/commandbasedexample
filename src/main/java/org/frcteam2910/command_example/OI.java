package org.frcteam2910.command_example;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import org.frcteam2910.command_example.commands.FullSpeedAheadCommand;
import org.frcteam2910.command_example.subsystems.DrivetrainSubsystem;

public class OI {
    private static Joystick joystick = new Joystick(0);

    public static Joystick getJoystick() {
        return joystick;
    }

    public static void bindButtons(DrivetrainSubsystem drivetrain) {
        JoystickButton fullSpeedButton = new JoystickButton(joystick, 0);

        fullSpeedButton.whileHeld(new FullSpeedAheadCommand(drivetrain));
    }
}
