package org.frcteam2910.command_example.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.frcteam2910.command_example.subsystems.DrivetrainSubsystem;

public class FullSpeedAheadCommand extends Command {

    private DrivetrainSubsystem drivetrain;

    public FullSpeedAheadCommand(DrivetrainSubsystem drivetrain) {
        this.drivetrain = drivetrain;

        requires(drivetrain);
    }

    @Override
    protected void initialize() {
        drivetrain.tankDrive(1.0, 1.0);
    }

    @Override
    protected boolean isFinished() {
        return false;
    }

    @Override
    protected void end() {
        drivetrain.tankDrive(0, 0);
    }
}
