package org.frcteam2910.command_example.commands;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.command.Command;
import org.frcteam2910.command_example.OI;
import org.frcteam2910.command_example.subsystems.DrivetrainSubsystem;

public class TankDriveCommand extends Command {
    private DrivetrainSubsystem drivetrain;

    public TankDriveCommand(DrivetrainSubsystem drivetrain) {
        this.drivetrain = drivetrain;

        requires(drivetrain);
    }

    @Override
    protected void execute() {
        double value = OI.getJoystick().getY(GenericHID.Hand.kLeft);

        drivetrain.tankDrive(value, value);
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
